﻿using iText.Html2pdf;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //index.html Convert to output.pdf
            using (FileStream htmlSource = File.Open("../../index.html", FileMode.Open))
            using (FileStream pdfDest = File.Open("../../output.pdf", FileMode.OpenOrCreate))
            {
                ConverterProperties converterProperties = new ConverterProperties();
                HtmlConverter.ConvertToPdf(htmlSource, pdfDest, converterProperties);
            }

            //merge three output.pdf
            string[] pdflist = new string[3];
            pdflist[0] = "output.pdf";
            pdflist[1] = "output.pdf";
            pdflist[2] = "output.pdf";
            new Program().mergePDFFiles(pdflist, "newpdf1.pdf");
        }
        public void mergePDFFiles(string[] fileList, string outMergeFile)
        {
            PdfReader reader;
            Document document = new Document();
            PdfWriter writer = PdfWriter.GetInstance(document, new FileStream("../../merge.pdf", FileMode.Create));
            document.Open();
            PdfContentByte cb = writer.DirectContent;
            PdfImportedPage newPage;
            for (int i = 0; i < fileList.Length; i++)
            {
                reader = new PdfReader("../../" + fileList[i]);
                int iPageNum = reader.NumberOfPages;
                for (int j = 1; j <= iPageNum; j++)
                {
                    document.NewPage();
                    newPage = writer.GetImportedPage(reader, j);
                    cb.AddTemplate(newPage, 0, 0);
                }
            }
            document.Close();
        }

    }
}
